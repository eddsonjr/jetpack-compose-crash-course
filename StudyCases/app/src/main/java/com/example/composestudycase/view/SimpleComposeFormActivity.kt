package com.example.composestudycase.view

import com.example.composestudycase.R
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Card
import androidx.compose.material.Surface
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composestudycase.view.composables.GenericButtonMaxWidth
import com.example.composestudycase.view.composables.OutLinedTextField

class SimpleComposeFormActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FormView()
        }
    }
}


@Composable
fun FormView(){
    val context = LocalContext.current


    Surface(modifier = Modifier
        .fillMaxSize()
        .padding(start = 8.dp, end = 8.dp, top = 12.dp, bottom = 12.dp)) {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        )
        {

            //Row relacionada a imagem
            Row(
                modifier = Modifier.wrapContentSize(),
                horizontalArrangement = Arrangement.Center
            )
            {
                Card(
                    modifier = Modifier.size(142.dp),
                    shape = CircleShape,
                    elevation = 2.dp
                ) {
                    Image(
                        modifier = Modifier
                            .width(128.dp)
                            .height(128.dp),
                        painter = painterResource(R.drawable.blake_face),
                        contentScale = ContentScale.Crop,
                        contentDescription = "")
                }
            }

            Spacer(modifier = Modifier.height(16.dp))

            //Nome
            OutLinedTextField(
                fieldLabel = context.getString(R.string.form_field_name),
                placeHolder = context.getString(R.string.form_field_name_placeHolder),
                icon = Icons.Default.Person,
                contentDescription = context.getString(R.string.form_field_name_placeHolder),
                keyboardType =  KeyboardOptions(keyboardType = KeyboardType.Text),
                modifier = Modifier.fillMaxWidth()
            )


            Spacer(modifier = Modifier.height(12.dp))

            //email
            OutLinedTextField(
                fieldLabel = context.getString(R.string.form_field_email),
                placeHolder = context.getString(R.string.form_field_email_placeHolder),
                icon = Icons.Default.Email,
                contentDescription = context.getString(R.string.form_field_email_placeHolder),
                keyboardType =  KeyboardOptions(keyboardType = KeyboardType.Email),
                modifier = Modifier.fillMaxWidth()
            )


            Spacer(modifier = Modifier.height(12.dp))

            //telefone
            OutLinedTextField(
                fieldLabel = context.getString(R.string.form_field_phone),
                placeHolder = context.getString(R.string.form_field_phone_placheHolder),
                icon = Icons.Default.Phone,
                contentDescription = context.getString(R.string.form_field_phone_placheHolder),
                keyboardType =  KeyboardOptions(keyboardType = KeyboardType.Phone),
                modifier = Modifier.fillMaxWidth()
            )


            Spacer(modifier = Modifier.height(12.dp))

            //password
            OutLinedTextField(
                fieldLabel = context.getString(R.string.form_field_password),
                placeHolder = "",
                icon = ImageVector.vectorResource(id = R.drawable.ic_key),
                contentDescription = "",
                keyboardType =  KeyboardOptions(keyboardType = KeyboardType.Password),
                isPassword = true,
                modifier = Modifier.fillMaxWidth()
            )



            Spacer(modifier = Modifier.height(12.dp))

            //repeat password
            OutLinedTextField(
                fieldLabel = context.getString(R.string.form_field_repeat_password),
                placeHolder = "",
                icon = ImageVector.vectorResource(id = R.drawable.ic_key),
                contentDescription = "",
                keyboardType =  KeyboardOptions(keyboardType = KeyboardType.Password),
                isPassword = true,
                modifier = Modifier.fillMaxWidth()
            )


            Spacer(modifier = Modifier.height(12.dp))

            //Botao de submissao
            GenericButtonMaxWidth("Submeter"){
                Toast.makeText(context,"Dados submetidos com sucesso!",Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

}


@Preview(showBackground = false)
@Composable
fun DefaultPreview2() {
    FormView()
}