package com.example.composestudycase.view.composables

import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue


@Composable
fun SimpleTextField(
    placeHolder: String,
    keyboardType: KeyboardOptions,
    isPassword: Boolean = false,

    modifier: Modifier )
{
    var text by remember { mutableStateOf(TextFieldValue("")) }

    if(isPassword){
        TextField(
            value = text,
            onValueChange = { text = it },
            keyboardOptions = keyboardType,
            maxLines = 1,
            singleLine = true,
            placeholder = { Text(text = placeHolder) },
            modifier = modifier,
            visualTransformation = PasswordVisualTransformation()
        )

    }else{
        TextField(
            value = text,
            onValueChange = { text = it },
            keyboardOptions = keyboardType,
            maxLines = 1,
            singleLine = true,
            placeholder = { Text(text = placeHolder) },
            modifier = modifier
        )
    }


}


@Composable
fun OutLinedTextField(
    fieldLabel: String,
    placeHolder: String,
    icon: ImageVector,
    contentDescription: String,
    keyboardType: KeyboardOptions,
    isPassword: Boolean = false,
    modifier: Modifier
)
{

    var text by remember { mutableStateOf(TextFieldValue("")) }

    if(isPassword){
        OutlinedTextField(
            value = text,
            modifier = modifier,
            leadingIcon = { Icon(imageVector = icon, contentDescription = contentDescription) },
            onValueChange =  { text = it },
            label = { Text(text = fieldLabel) },
            keyboardOptions = keyboardType,
            maxLines = 1,
            singleLine = true,
            visualTransformation = PasswordVisualTransformation(),
            placeholder = { Text(text = placeHolder) }

        )
    }else{
        OutlinedTextField(
            value = text,
            modifier = modifier,
            leadingIcon = { Icon(imageVector = icon, contentDescription = contentDescription) },
            onValueChange = { text = it },
            label = { Text(text = fieldLabel) },
            keyboardOptions = keyboardType,
            maxLines = 1,
            singleLine = true,
            placeholder = { Text(text = placeHolder) }

        )
    }
}




