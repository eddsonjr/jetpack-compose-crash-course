package com.example.composestudycase.view.composables.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


//Cores customizadas para usar no aplicativo
val ColorMain = Color(red = 68, green = 119, blue = 201)
val ColorMainLight = Color(red = 108, green = 137, blue = 184)
val ColorMainDark = Color(red = 3, green = 58, blue = 148)
val ColorAlert = Color(red = 169, green = 69, blue = 58)
val ColorLightGrey = Color(red = 210, green =  210,blue = 210)