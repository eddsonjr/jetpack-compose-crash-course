package com.example.composestudycase.utils

import android.content.Context

class UIUtils {
    companion object {

        /**
         * Get the screen dimensions in DP. The return is a Pair of Float values: Width and Height
         * @param context
         * @return Pair<Float,Float>
         * */
        fun getDeviceScreenDimensionInDP(context: Context): Pair<Float,Float> {
            val widthDp = context.resources.displayMetrics.run { widthPixels / density }
            val heightDp = context.resources.displayMetrics.run { heightPixels / density }
            return Pair(widthDp,heightDp)

        }
    }

}