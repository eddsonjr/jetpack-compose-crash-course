package com.example.composestudycase.view


import com.example.composestudycase.R
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.example.composestudycase.view.composables.GenericSurface
import com.example.composestudycase.view.composables.GenericButtonMaxWidth
import com.example.composestudycase.view.composables.ui.theme.ColorLightGrey
import com.example.composestudycase.view.composables.ui.theme.ComposeStudyCaseTheme

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeStudyCaseTheme {
               mainScreenViewSurface()
            }
        }
    }
}


@Composable
fun mainScreenViewSurface() {

    val context = LocalContext.current

    GenericSurface(modifier = Modifier.fillMaxSize(), color = ColorLightGrey) {

        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally)
        {



            GenericButtonMaxWidth(text = context.getString(R.string.compose_formulario_simples))
            {
                context.startActivity(Intent(context, SimpleComposeFormActivity::class.java))
            }

            GenericButtonMaxWidth(text = context.getString(R.string.cmpose_formulario_simples2))
            {
                context.startActivity(Intent(context, SimpleComposeFormActivity2::class.java))
            }


            GenericButtonMaxWidth(text = context.getString(R.string.simple_list_with_navigation))
            {
                //nada para fazer por hora
            }

        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    mainScreenViewSurface()
}


