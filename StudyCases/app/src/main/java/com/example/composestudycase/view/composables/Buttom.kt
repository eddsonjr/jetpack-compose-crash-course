package com.example.composestudycase.view.composables

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp


@Composable
fun GenericButton(text: String, onClickListener: () -> Unit) {
        Button(onClick = { onClickListener() }) {
            Text(text = text,
                color = Color.White,
                fontFamily = FontFamily.Default,
                textAlign = TextAlign.Center,
                fontSize = textFontSizeDefault)
        }
}


@Composable
fun RoundedIconButton(
    icon: Int,
    contentDescription: String,
    iconColor: Color,
    borderStrokeColor: Color,
    onClickListener: () -> Unit){

    OutlinedButton(
        onClick = { onClickListener() },
        modifier= Modifier.size(60.dp),  //avoid the oval shape
        shape = CircleShape,
        border= BorderStroke(1.dp, borderStrokeColor),
        contentPadding = PaddingValues(0.dp),  //avoid the little icon
        colors = ButtonDefaults.outlinedButtonColors(contentColor =  iconColor)
    ) {
        Icon(painter = painterResource(id = icon), contentDescription = contentDescription)
    }


}





@Composable
fun GenericButtonMaxWidth(text: String, onClickListener: () -> Unit) {
    Button(
        modifier = Modifier
            .fillMaxWidth(),
        onClick = { onClickListener() }) {
        Text(text = text,
            color = Color.White,
            fontFamily = FontFamily.Default,
            textAlign = TextAlign.Center,
            fontSize = textFontSizeDefault)
    }
}
