package com.example.composestudycase.view


import com.example.composestudycase.R
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composestudycase.view.composables.RoundedIconButton
import com.example.composestudycase.view.composables.SimpleTextField
import com.example.composestudycase.view.composables.ui.theme.ComposeStudyCaseTheme

class SimpleComposeFormActivity2 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeStudyCaseTheme {
                // A surface container using the 'background' color from the theme
                FormView2()
            }
        }
    }
}


@Composable
fun FormView2() {

    val context = LocalContext.current

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp))
    {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState()),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally)
        {

            Image(
                modifier = Modifier
                    .width(128.dp)
                    .height(128.dp),
                painter = painterResource(R.drawable.blake_face),
                contentScale = ContentScale.Crop,
                contentDescription = "")

            Spacer(modifier = Modifier.height(12.dp))

            RoundedIconButton(
                icon = R.drawable.ic_camera,
                contentDescription = "Tirar Foto",
                iconColor = Color.LightGray,
                borderStrokeColor = Color.LightGray
            ) {
                //TODO - chamar imagepicker
            }


            //Nome
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(start = 12.dp, end = 12.dp, top = 8.dp, bottom = 8.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            )

            {

                Text(text = "Nome: ")

                SimpleTextField(
                    placeHolder = "Insira o seu nome",
                    keyboardType = KeyboardOptions(keyboardType = KeyboardType.Text),
                    isPassword = false,
                    modifier = Modifier.fillMaxWidth()
                )
            }



            //Telefone
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(start = 12.dp, end = 12.dp, top = 8.dp, bottom = 8.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            )

            {

                Text(text = "Phone: ")

                SimpleTextField(
                    placeHolder = "(99)9999-9999",
                    keyboardType = KeyboardOptions(keyboardType = KeyboardType.Phone),
                    isPassword = false,
                    modifier = Modifier.fillMaxWidth()
                )
            }


            //Email
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(start = 12.dp, end = 12.dp, top = 8.dp, bottom = 8.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            )

            {

                Text(text = "Email: ")

                SimpleTextField(
                    placeHolder = "email@email.com",
                    keyboardType = KeyboardOptions(keyboardType = KeyboardType.Email),
                    isPassword = false,
                    modifier = Modifier.fillMaxWidth()
                )
            }


            //Password
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(start = 12.dp, end = 12.dp, top = 8.dp, bottom = 8.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            )

            {

                Text(text = "Password: ")

                SimpleTextField(
                    placeHolder = "",
                    keyboardType = KeyboardOptions(keyboardType = KeyboardType.Email),
                    isPassword = true,
                    modifier = Modifier.fillMaxWidth()
                )
            }



            //Password - Repetir
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(start = 12.dp, end = 12.dp, top = 8.dp, bottom = 8.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceAround
            )

            {

                Text(text = "Psw rep.:: ")

                SimpleTextField(
                    placeHolder = "",
                    keyboardType = KeyboardOptions(keyboardType = KeyboardType.Email),
                    isPassword = true,
                    modifier = Modifier.fillMaxWidth()
                )
            }


            RoundedIconButton(
                icon = R.drawable.ic_check,
                contentDescription = "Finalizar cadastro",
                borderStrokeColor = Color.Green,
                iconColor = Color.Green
            ) {

                Toast.makeText(context,"Cadastro finalizado com sucesso",Toast.LENGTH_SHORT)
                    .show()

            }
        }
    }

}


@Preview(showBackground = true)
@Composable
fun DefaultPreview3() {
    FormView2()
}