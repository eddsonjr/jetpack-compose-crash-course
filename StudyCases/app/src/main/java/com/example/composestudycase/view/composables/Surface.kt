package com.example.composestudycase.view.composables

import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

@Composable
fun GenericSurface(
    color: Color? = null,
    modifier: Modifier,
    content: @Composable () -> Unit) {

    Surface(
        color = color ?: Color.Unspecified,
        modifier = modifier)
    {
        content()
    }
}