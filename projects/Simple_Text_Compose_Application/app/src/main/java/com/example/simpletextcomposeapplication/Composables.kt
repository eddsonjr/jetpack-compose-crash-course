package com.example.simpletextcomposeapplication

import android.content.Context
import android.graphics.Color.alpha
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

/*
* Você pode criar arquivos kotlin que contem somente os componentes elementos de UI (funções
* composable). Isso permite uma maior flexibilidade na reutilização de componentes gráficos.
*
* */

const val IMAGE_DESCRIPTION = "Jetpack compose image"
const val HELLO_TEXT = "Hello Jetpack compose!"

fun showToast(context: Context){
    Toast.makeText(context,"Hello Compose. Button Clicked", Toast.LENGTH_SHORT).show()
}


@Composable
fun showButton() {
    val context = LocalContext.current
    Button(onClick = {
        showToast(context)
    }) {
        Text(text = "Click Me!")
    }
}

/*
* RowScope é basicamente um espaço horizontal no qual você pode adicionar outros
* components composables.
*
* Por exemplo, no botão acima, você pode ver que dentro do bloco do RowScope, há um
* Text(), que por definição é um outro composable. Você também pode usar outras funções que tenha
* criado, desde que ela tenha o @Composable associado (seja um composable).
* Você pode combinar vários composables dentro do RowScope a fim de criar um componente mais
* customizado.
*
* */



@Composable
fun showHelloIcon() {
    androidx.compose.foundation.Image(
        painter = painterResource(id = R.drawable.ic_android),
        contentDescription = IMAGE_DESCRIPTION)
}


@Composable
fun helloButtonWithImage(disabled: Boolean = false) {
    val context = LocalContext.current
    Button(onClick = { showToast(context) },

        //trocando a cor do botão
        colors = ButtonDefaults.buttonColors(backgroundColor = Color.Yellow),

        modifier = Modifier
            .fillMaxWidth(fraction = 0.75f)
            .alpha(0.6f)
    )
    {
        Text(text = HELLO_TEXT ) //lembre-se que o RowScope gera um scopo horizontal, logo os elementos são dispostos horizontalmente
        showHelloIcon() //perceba que aqui estou usando uma outra funcao que esta assinada como Composable

    }
}


@Composable
fun MyComposable() {
    Text("Hi there!",
        Modifier
            .border(2.dp, Color.Green)
            .padding(50.dp)
            .border(2.dp, Color.Red)
            .padding(50.dp)
    )
}

/*
* Modifier = responsável por permitir acessar e alterar determinados
* atributos dos elementos composables, como por exemplo width,heigh,
* alpha, background, entre outros.
*
* Para o width e height, você pode usar os atributos .width() / .height ou .size().
* Caso use o .size(xx.dp), tanto o width quanto o height irão ter o mesmo valor.
*
* Já o .fillMaxSize() irá maximizar o composable (eles sozinho na tela, ao usar tal propiedade, irá
* ocupar a tela inteira).  Você também pode usar o .fillMaxWidth() e o .fillMaxHeight()
* e ao usar os métodos acima, você pode passar o valor de fraction, que está relacionado a
* "porcentagem" que aquele elemento irá ocupar na tela (ex: 0.5 ocupa metade da tela)
*
*
*
* */



/*
* Obs: quando você der dois cliques no preview no componente que está sendo visualizado, a IDE
* irá lhe levar
*
* */