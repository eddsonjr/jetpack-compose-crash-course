package com.example.simpletextcomposeapplication

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.simpletextcomposeapplication.ui.theme.SimpleTextComposeApplicationTheme

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /*
        * setContent e Funções e Composição.
        *
        * Funções de composição: essas funções permitem que você defina a IU do app de
        * maneira programática, descrevendo as dependências de dados e de formas dela, em vez de se
        * concentrar no processo de construção da IU
        * (inicializando um elemento, anexando-o a um pai etc.). Para criar uma função que pode ser
        * composta, basta adicionar a anotação @Composable ao nome da função. Funções composables
        * devem ser chamadas por outras funções do mesmo tipo.
        *
        * O Android Compose é baseado em composables.
        *
        * setContent: Define o layout da atividade em que as funções de composição são chamadas.
        * Elas só podem ser chamadas usando outras funções desse tipo. Basicamente
        * serve para renderizar a tela atraves da chamada de funções de composição.
        *
        * Os widgets, tipo Button, Text, etc, são composables, logo você pode passá-los diretamente
        * dentro do setContent {} diretamente.
        *
        *
        *
        * */

        //outra maneira de usar o setContent, sem que seja via lambda
//        setContent(content = { Greeting(name = "Android Compose") })

        setContent {

            Greeting("Android Android Compose")
//            helloButtonWithImage(true)

//            SimpleTextComposeApplicationTheme {
//                // A surface container using the 'background' color from the theme
//                Surface(
//                    modifier = Modifier.fillMaxSize(),
//                    color = MaterialTheme.colors.background
//                ) {
//                    Greeting("Android")
//                }
//            }
        }
    }
}


@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!",

        //estilo feito manualmente:
//        style = TextStyle(
//            color = Color.Blue,
//            fontWeight = FontWeight.Bold,
//            fontSize = 16.sp)

        style = MaterialTheme.typography.body1, //estilo baseado na tipografia do google
        modifier = Modifier
            .width(200.dp)
            .height(240.dp)
            .padding(start = 4.dp, end = 4.dp, top = 4.dp, bottom = 4.dp)
            .clickable { }

    )

}


/*
* Modifier = responsável por permitir acessar e alterar determinados
* atributos dos elementos composables, como por exemplo width,heigh,
* alpha, background, entre outros.
*
* Através do modifier, você também consegue adicionar suporte ao click, invocando o
* .clickable().
*
* É possível adicionar também padding, podendo escolher entre adicionar o pading em todas as
* direções, simplesmente usando padding(xx.dp), ou colocar em determinada direção, através de
* start, end, top ou bottom. Não esqueca sempre de dar um refresh no preview para aplicar tais
* mudanças.
*
*
* Importante: a forma como o Modifier é criado importa! O componente que está sendo desenvolvido
* pode ter seu comportamento alterado de acordo com a ordem dos atributos do modifier. Por exemplo,
* alterar a ordem do padding e do evento de click pode acabar gerando resultados diferentes no
* elemento composable.  Ao colocar o padding antes do evento de click, o evento de click será
* dado sobre a área na qual o padding está sendo aplicado. Caso coloque o padding após o click,
* o evento de click irá descorrer sobre toda a área do widget, e somente depois o padding será
* aplicado.
*
* Caso seja alterado também a ordem do padding em conjunto com o width e o height, como por exemplo,
* aplicar o padding antes do width/height, o padding será aplicado não surtirá e feito sobre o
* tamanho da área do widget.
*
*
* Importante: no caso de textos, mesmo que seja usado uma tipografia fornecida pelo google,
* você ainda pode alterar alguns atributos, como por exemplo, FontWeight, entre outros.
*
* Convencionalmente as funções composable iniciam com a primeira letra do nome em maiúsculo.
* */




//Este bloco serve para renderizar a UI que será mostrada no preview.
//Nao esqueca de atualizar o preview toda vez que alterar o setContent {}
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {

    SimpleTextComposeApplicationTheme {
        MyComposable()
//        Greeting("Android Compose!")
//        showButton()
//        helloButtonWithImage()
    }
}