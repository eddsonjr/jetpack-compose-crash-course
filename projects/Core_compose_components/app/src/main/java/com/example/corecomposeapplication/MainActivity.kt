package com.example.corecomposeapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainScreen()
        }
    }
}

@Composable
fun MainScreen() {

    //Este surface para a tela inteira
    Surface(color = Color.DarkGray, modifier = Modifier.fillMaxSize()) {

        //Surface aninhado e internamente a ela um Text
        Surface(color = Color.Black, modifier = Modifier.wrapContentSize(
            align = Alignment.TopEnd
        )) {
            Text(
                style = MaterialTheme.typography.h4,
                color = Color.White,
                text = "Wrapped content"
            )
        }

    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
//    ComposableInsideOtherWithoutArrangement()
//    ComposableWithRow()
//    ComposableWithColumn()
    ComposableRowWithColumn()
}


/*
* Surface
* -------
*
* Trata-se de um dos principais componentes do Compose e está relacionado ao Material Design.
*
* Cada Surface existe em uma determinada elevação no layout, o que influencia como esse pedaço
* de superficie se relaciona visualmente com demais superficies.
* Com o Surface é possível:
*  1. O próprio surface é responsável por gerar uma sombra em sua elevação.
*  2. Trabalhar bordas na superficie
*  3. Trabalhar com cor de fundo da superfície
*  4. Trabalhar com um shape para a superfície e seus filhos
*  5. Bloquear eventos de click para os demais elementos dentro da superfície
*
*  O Surface pode ser considerado como o antigo View do android (xml).
*  Você pode ter um Surface dentro de outro.
*
*  Modifier
* ---------
*
* Quando se tem um Surface com o modifier: modifier = Modifier.fillMaxSize(),
* isso indica que aquela superfície irá ocupar toda a tela.
* Quando se tem um Composable com o modifier para wrapContentSize(),
* então o uso da tela sera proporcional ao necessário por aquele widget.
* Dentro do wrapContentSize() é possível especificar o alinhamento, sendo o
* Center o alinhamento padrão usado pelo Compose. Algo que se tem que tomar
* certo cuidado e atenção referente ao alinhamento é que alguns alinhamentos não
* são aceitos, pois eles apresentam o alinhamento baseado em somente um eixo
* (vertical ou horizontal) e quando isso ocorre, o compose não consegue realizar a orientação
* referente ao eixo que não foi informado. Por exemplo, CenterVertically indica o alinhamento
* somente em um dos eixos e como o Compose não consegue entender como alinhar no eixo
* horizontal, isso acaba gerando um erro.
*
*
* */

