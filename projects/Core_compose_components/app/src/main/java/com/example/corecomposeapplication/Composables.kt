package com.example.corecomposeapplication

import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp


@Composable
fun ComposableInsideOtherWithoutArrangement() {

    //Este surface para a tela inteira
    Surface(color = Color.DarkGray, modifier = Modifier.fillMaxSize()) {

        //Surface aninhado e internamente a ela um Text
        Surface(color = Color.Black, modifier = Modifier.wrapContentSize(
            align = Alignment.TopCenter)) {
            Text(
                style = MaterialTheme.typography.h3,
                color = Color.White,
                text = "Wrapped content"
            )

            Text(
                style = MaterialTheme.typography.h3,
                color = Color.White,
                text = "Second Composable Here"
            )
        }

    }
}

/*
* Quando você tem um composable formado por outros composables e você não especifica como
* esses composables serão dipsostos na tela, o Compose tende a colocá-los sobrepostos.
*
* Para ordenar os composables e dispo-los de maneira ordenada, o Compose conta com o conceito de
* Column, Row e Box.
*
* Column = Organiza os composables em sentido vertical (vertical container).
* Row = Organiza os composables em sentido horizontal (horizontal container).
* Box = permite organizar os composables sobrepondo-os e manipulando suas respectivas orientações.
*
*
*
* */



/*
* Perceba que na funcao baixo sera aplicado o conceito de reuso de componentes, sendo este
* conceito uma das principais vantagens do uso do Compose
* */

@Composable
fun ComposableWithRow() {
    Surface(color = Color.DarkGray, modifier = Modifier.fillMaxSize()) {

        //lembre-se que row esta relacionado ao espacamento horizontal.
        Row(modifier = Modifier.fillMaxSize(),
            horizontalArrangement = Arrangement.SpaceAround,
            verticalAlignment = Alignment.Bottom
        ) {
            HorizontalColoredBar(
                color = Color.Yellow,
                content = { SimpleText(text = "Row one!") })

            HorizontalColoredBar(
                color = Color.Magenta,
                content = { SimpleText(text = "Row two!") })


            HorizontalColoredBar(
                color = Color.Red,
                content = { SimpleText(text = "Row three!") })
        }
    }
}


@Composable
fun ComposableWithColumn() {
    Surface(color = Color.DarkGray, modifier = Modifier.fillMaxSize()) {

        //lembre-se que row esta relacionado ao espacamento horizontal.
        Column(modifier = Modifier.wrapContentSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            HorizontalColoredBar(
                color = Color.Yellow,
                content = { SimpleText(text = "Column one!") })

            HorizontalColoredBar(
                color = Color.Magenta,
                content = { SimpleText(text = "Column two!") })


            HorizontalColoredBar(
                color = Color.Red,
                content = { SimpleText(text = "Column three!") })
        }
    }
}


@Composable
fun ComposableRowWithColumn() {

    Surface(color = Color.DarkGray, modifier = Modifier.fillMaxSize()) {

        //lembre-se que row esta relacionado ao espacamento horizontal.
        Column(modifier = Modifier.wrapContentSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceAround,
                verticalAlignment = Alignment.Bottom) {
                HorizontalColoredBar(
                    color = Color.Yellow,
                    content = { SimpleText(text = "Row one!") })

                HorizontalColoredBar(
                    color = Color.Magenta,
                    content = { SimpleText(text = "Row two!") })
            }


            HorizontalColoredBar(
                color = Color.Red,
                content = { SimpleText(text = "Column one!") })


        }
    }

}


/*
* Organizando os widgets nos sentidos horizontal e vertical
*
* Row: Quando se utilizar row, o compose dispõe os widgets através do horizontalArrangement. Caso
* queira trabalhar o alinhamento na vertical, você utilizar o verticalAlignment.
*
* Column: Quando se utilizar o column, o compose dispõe os widgets atraves do verticalArrangement.
* Caso queira trabalhar o alinhamento na horizonta, você utiliza o horizontalAlignment.
*
* */
