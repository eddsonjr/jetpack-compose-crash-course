package com.example.corecomposeapplication

import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

/*
* Você pode reutilizar composables, sendo esta uma das principais vantagens do uso do composable.
* */

@Composable
fun HorizontalColoredBar(color: Color?, content: @Composable () -> Unit) {
    Surface(
        color = color ?: Color.Unspecified,
        modifier = Modifier
            .height(120.dp)
            .width(120.dp)
    ){
        content()
    }
}


@Composable
fun SimpleText(text: String) {
     Text(
        text = text,
        fontSize = 12.sp)
}
