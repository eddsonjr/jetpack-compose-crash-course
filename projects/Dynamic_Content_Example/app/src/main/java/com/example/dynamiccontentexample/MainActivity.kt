package com.example.dynamiccontentexample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel = MainViewModel()
        setContent {
            MainScreen(viewModel)
        }
    }
}

@Composable
fun MainScreen(viewModel: MainViewModel = MainViewModel()) {
    val newNameStateContent = viewModel.textFieldState.observeAsState("")

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        GreetingMessage(
            newNameStateContent.value
        ) { newName -> viewModel.onTextChanged(newName) }
    }
}

@Composable
fun GreetingMessage(
    textFieldValue: String,
    textFieldUpdate: (newName: String) -> Unit
) {

    TextField(value = textFieldValue, onValueChange = textFieldUpdate)
    Button(onClick = { }) {
        Text(textFieldValue)
    }
}

@Composable
fun Greeting(name: String) {
    Text(
        style = MaterialTheme.typography.h5,
        text = "Hello $name!")
}

/*
* O Jetpack compose permite a criação de conteúdo na view de forma dinâmica. No código acima, note
* que existe um método GreetingList, que recebe como parâmetro uma lista de nomes. Esse método é
* um composable e tem como objetivo percorrer a lista e printar os nomes na tela. No entanto,
* para cada print na tela, é chamado o metodo Greeting, que é do tipo composable também.
*
* */

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {

    MainScreen()
}



/*
* O Jetpack compose trabalha com os conceitos de State e Recomposition.
*
* State significa basicamente que qualquer valor dentro da aplicação pode mudar a qualquer momento.
* O Jetpack compose trabalha com a ideia de UIFlow, no qual o sentido em looping é:
*   Event -> Update State -> Display Event
*
* Estes 3 estados ocorrem em looping, quando termina o Display Event, volta para o Event. Quando
* se quer alterar os dados de que são utilizados em algum composable, deve-se ter em mente que o
* composable terá o seu estado atual alterado.
* Quando se vai passar um novo dado para um composable, deve-se dizer que o composable deve
* 'relembrar' dos seus estados (cadas estado com uma sua respectiva variável) e para tanto
* utiliza-se a notação parecida com:
*
*   val greetingListState = remember { mutableStateListOf<String>("John","Amanda") }
*
*
* */