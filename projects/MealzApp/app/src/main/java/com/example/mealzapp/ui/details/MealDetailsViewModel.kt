package com.example.mealzapp.ui.details

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.mealzapp.model.MealsRepository
import com.example.mealzapp.model.response.MealResponse
import com.example.mealzapp.ui.details.Constants.MEAL_CATEGORY_ID

class MealDetailsViewModel(private val savedStateHandle: SavedStateHandle) : ViewModel() {

    private val repository: MealsRepository = MealsRepository.getInstance()

    var mealsState = mutableStateOf<MealResponse?>(null)

    init {
        val mealId = savedStateHandle.get<String>(MEAL_CATEGORY_ID)?: ""
        mealsState.value = repository.getMeal(mealId)
    }


}