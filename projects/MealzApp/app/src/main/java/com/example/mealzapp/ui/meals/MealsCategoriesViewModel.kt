package com.example.mealzapp.ui.meals

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.mealzapp.model.MealsRepository
import com.example.mealzapp.model.response.MealResponse
import kotlinx.coroutines.*

class MealsCategoriesViewModel(private val repository: MealsRepository = MealsRepository.getInstance()):
    ViewModel() {

//    private val mealsJob = Job()
//    init {
//        val scope = CoroutineScope(mealsJob + Dispatchers.IO)
//
//        scope.launch(){ //pode-se tambem usar viewmodelScope
//            val mealsCategories = getMealsCategories()
//            rememberedMealsCategories.value = mealsCategories
//        }
//    }


    init {

        viewModelScope.launch(Dispatchers.IO){
            val mealsCategories = getMealsCategories()
            rememberedMealsCategories.value = mealsCategories
        }
    }



    val rememberedMealsCategories: MutableState<List<MealResponse>> =
        mutableStateOf(emptyList<MealResponse>())


//    //e chamado quando a view model e destruida
//    override fun onCleared() {
//        super.onCleared()
//        mealsJob.cancel()
//    }

    private suspend fun getMealsCategories(): List<MealResponse>{
        return  repository.getMealsCategories().categories
    }

}