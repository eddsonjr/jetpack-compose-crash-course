package com.example.mealzapp.ui.details

object Constants {

    const val DESTINATION_MEALS_LIST_ROUTE = "destination_meals_list"
    const val DESTINATION_MEAL_DETAILS = "destination_meal_details"
    const val FOOD_CATEGORY_ID = "food_category_id"
    const val MEAL_CATEGORY_ID = "meal_category_id"
}