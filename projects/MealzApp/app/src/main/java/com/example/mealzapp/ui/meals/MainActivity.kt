package com.example.mealzapp.ui.meals

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.mealzapp.ui.details.Constants.DESTINATION_MEALS_LIST_ROUTE
import com.example.mealzapp.ui.details.Constants.DESTINATION_MEAL_DETAILS
import com.example.mealzapp.ui.details.Constants.FOOD_CATEGORY_ID
import com.example.mealzapp.ui.details.Constants.MEAL_CATEGORY_ID
import com.example.mealzapp.ui.details.MealDetailsScreen
import com.example.mealzapp.ui.details.MealDetailsViewModel
import com.example.mealzapp.ui.theme.MealzAppTheme


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MealzAppTheme {
                FoodiezApp()
            }
        }
    }
}


@Composable
private fun FoodiezApp() {
    val navigationController = rememberNavController()
    NavHost(navController = navigationController, startDestination = DESTINATION_MEALS_LIST_ROUTE ){
        composable(route = DESTINATION_MEALS_LIST_ROUTE) {
            MealsCategoriesScreen() { navigationMealId ->
                navigationController.navigate(route = "$DESTINATION_MEAL_DETAILS/$navigationMealId")

            }
        }
        composable(route = "$DESTINATION_MEAL_DETAILS/{$MEAL_CATEGORY_ID}",
            arguments = listOf(navArgument(MEAL_CATEGORY_ID){
                type = NavType.StringType
            })) {
                val viewModel: MealDetailsViewModel = viewModel()
                MealDetailsScreen(meal = viewModel.mealsState.value)
        }
    }
}
