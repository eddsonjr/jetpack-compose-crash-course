package com.example.mealzapp.model.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class MealsCategoriesResponse(
    val categories: List<MealResponse>
): Serializable


data class MealResponse(

    @SerializedName("idCategory")
    val id: String,

    @SerializedName("strCategory")
    val name: String,

    @SerializedName("strCategoryThumb")
    val imageUrl: String,


    @SerializedName("strCategoryDescription")
    val description: String
): Serializable

