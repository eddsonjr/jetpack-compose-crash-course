package com.example.profilecardlayout

data class UserProfile (
    val id: Int,
    val name: String,
    val status: Boolean,
    val imgUrl: String)


val userProfileList = arrayListOf<UserProfile>(
    UserProfile(0,"Blake Belladona", true, "https://preview.redd.it/tgw0o11qsf231.jpg?auto=webp&s=1840578b95da8c1aa9760babe8d3f68bf7e6d8ed"),
    UserProfile(1,"Ruby Rose", false, "https://pbs.twimg.com/media/FDBLRpFWQAAsyOA.jpg"),
    UserProfile(2,"Yang Xiao", true, "https://i.pinimg.com/originals/8b/bf/42/8bbf42402a898a39b80be177d120994a.jpg"),
    UserProfile(3,"Weiss Scheene", false, "https://i.pinimg.com/736x/53/0e/bd/530ebda899619ec984170fc52ea58691.jpg"),
    UserProfile(4,"Shirley Fenette", true,"https://cdn.myanimelist.net/images/characters/2/39320.jpg"),
    UserProfile(5,"Chun li",true,"https://i.pinimg.com/originals/d2/ad/c2/d2adc2e69d7792253e9bba3437f48679.jpg"),
    UserProfile(6,"Marine",false,"https://static.doomworld.com/monthly_2019_05/image.png.baafc1ea79f288ab1bb77d4ac519c8db.png"),
    UserProfile(7,"Arthur Morgan",false,"https://i.pinimg.com/736x/25/5c/0e/255c0e48f6b52829840fea72c68f78cb.jpg")
)
