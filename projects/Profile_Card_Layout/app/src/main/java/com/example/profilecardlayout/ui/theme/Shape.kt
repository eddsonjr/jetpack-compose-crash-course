package com.example.profilecardlayout.ui.theme

import android.graphics.drawable.shapes.Shape
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.ui.unit.dp

val Shapes = Shapes(
    small = RoundedCornerShape(4.dp),
    medium = CutCornerShape(topEnd = 12.dp),
    large = RoundedCornerShape(0.dp)
)
