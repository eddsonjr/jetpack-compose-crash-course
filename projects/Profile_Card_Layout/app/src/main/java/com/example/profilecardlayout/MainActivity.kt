package com.example.profilecardlayout

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.profilecardlayout.ui.theme.MyTheme
import com.example.profilecardlayout.ui.theme.lightGreen

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyTheme {
                UsersApplication()
            }

        }
    }
}


@Composable
fun UsersApplication(userProfiles: List<UserProfile> = userProfileList) {
    val navigationController = rememberNavController()
    NavHost(navController = navigationController, startDestination = "users_list"){
        composable(route = "users_list"){
            UserListScreen(userProfiles, navigationController)
        }
        composable(route = "user_details/{userId}",
            arguments = listOf(navArgument("userId") {
                type = NavType.IntType
        })){ navStackEntry ->
            UserProfileDetailScreen(
                navStackEntry.arguments!!.getInt("userId"),
                navigationController)
        }
    }
}

@Composable
fun AppBar(title: String, icon: ImageVector, iconClickAction: () -> Unit) {
    TopAppBar(
        navigationIcon = {
            Icon(
                imageVector = icon,
                contentDescription = "",
                modifier = Modifier
                    .padding(horizontal = 12.dp)
                    .clickable { iconClickAction.invoke() }
            ) },
        title = { Text(text = title)}
    )
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun UserListScreen(userProfiles: List<UserProfile>,navController: NavController?) {
    
    Scaffold(topBar = { AppBar(
        title = "User List",
        icon = Icons.Default.Home
    ) { }
    }){
        Surface(
            modifier = Modifier.fillMaxSize())
        {
            LazyColumn() {
                items(userProfiles) { userProfile ->
                    ProfileCard(userProfile = userProfile){
                        navController?.navigate("user_details/${userProfile.id}")
                    }
                }
            }
        }
    }
}



@Composable
fun ProfileCard(userProfile: UserProfile, clickAction: () -> Unit) {
    Card(

        modifier = Modifier
            .padding(top = 8.dp, bottom = 4.dp, start = 16.dp, end = 16.dp)
            .fillMaxWidth()
            .wrapContentHeight(align = Alignment.Top)
            .clickable { clickAction.invoke() },
        elevation = 8.dp,
        backgroundColor = Color.White
    )
    {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start)
        {
            ProfilePicture(userProfile.imgUrl,userProfile.status,72.dp)
            ProfileContent(userProfile.name,userProfile.status,Alignment.Start)
        }

    }
}


@Composable
fun ProfilePicture(imgUrl: String, onlineStatus: Boolean,imageSize: Dp) {

    Card(
        shape = CircleShape,
        border = BorderStroke(
            width = 2.dp,
            color = if(onlineStatus) MaterialTheme.colors.lightGreen else Color.Red),
        modifier = Modifier.padding(8.dp),
        elevation = 4.dp)
    {


        //usando coil para carregar as imagens
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data(imgUrl)
                .crossfade(true)
                .build(),
            placeholder = painterResource(R.drawable.ic_placeholder),
            contentDescription = "user picture",
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(imageSize)
                .clip(CircleShape)

        )

        //Load image from resources using compose (with resources)
//        Image(
//            painter = painterResource(id = drawableId),
//            contentDescription = "Content description",
//            modifier = Modifier.size(72.dp),
//            contentScale = ContentScale.Crop
//        )
    }


}


@Composable
fun ProfileContent(userName: String,onlineStatus: Boolean, alignment: Alignment.Horizontal) {

    Column(modifier = Modifier
        .padding(8.dp),
        horizontalAlignment = alignment
    )
    {
        Text(
            text = userName,
            style = MaterialTheme.typography.h5
        )
        Text(
            text = if(onlineStatus) "Active now" else "Offline",
            modifier = Modifier.alpha(0.5f),
            style = MaterialTheme.typography.body2,
        )
    }

}



@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun UserProfileDetailScreen(userId: Int, navController: NavController?) {

    val userProfile = userProfileList[userId]

    Scaffold(topBar = {
        AppBar(
            title = "User profile detail",
            icon = Icons.Default.ArrowBack
    ) {
            navController?.navigateUp()

        }

    }){
        Surface(
            modifier = Modifier.fillMaxSize())
        {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top)
            {
                ProfilePicture(userProfile.imgUrl,userProfile.status,168.dp)
                ProfileContent(userProfile.name,userProfile.status,Alignment.CenterHorizontally)
            }
        }
    }
}



@Preview(showBackground = true)
@Composable
fun UserListPreview() {
    MyTheme {
        UserListScreen(userProfileList,null)
    }
}



@Preview(showBackground = true)
@Composable
fun UserProfileDetailPreview() {
    MyTheme {
        UserProfileDetailScreen(userId = 0,null)
    }
}