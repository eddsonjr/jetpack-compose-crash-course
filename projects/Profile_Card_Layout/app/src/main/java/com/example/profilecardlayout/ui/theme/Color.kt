package com.example.profilecardlayout.ui.theme


import androidx.compose.material.Colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


val veryLightGrey = Color(0x60DCDCDCDC)
val lightGreen200 = Color(0x9932CD32)


//Voce pode usar kotlin extensions para definir as cores dentro do Colors
//Com isso ele passa a ser uma cor disponivel no MaterialTheme, onde voce pode usar da seguinte
//maneira: MaterialTheme.colors.lightGreen
val Colors.lightGreen: Color
    @Composable
    get() = lightGreen200